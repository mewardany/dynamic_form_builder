<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $form_id
 *
 * @property DynamicForm $form
 * @property PostDetails[] $postDetails
 */
class Post extends \yii\db\ActiveRecord
{
    public function __construct($config = array()) {
        $this->form_id = 1;
        parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id'], 'required'],
            [['form_id'], 'integer'],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => \wardany\dform\models\DynamicForm::className(), 'targetAttribute' => ['form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_id' => 'Form ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(DynamicForm::className(), ['id' => 'form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostDetails()
    {
        return $this->hasMany(PostDetails::className(), ['post_id' => 'id']);
    }
    
    
    public function behaviors() {
        return[
        \wardany\dform\behaviors\DynamicFormBuilderBehavior::className()
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "post_details".
 *
 * @property int $id
 * @property int $post_id
 * @property int $input_id
 * @property string $value
 *
 * @property DynamicFormInputs $input
 * @property Post $post
 */
class PostDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'input_id'], 'integer'],
            [['value'], 'string'],
            [['input_id'], 'exist', 'skipOnError' => true, 'targetClass' => DynamicFormInputs::className(), 'targetAttribute' => ['input_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'input_id' => 'Input ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInput()
    {
        return $this->hasOne(DynamicFormInputs::className(), ['id' => 'input_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}

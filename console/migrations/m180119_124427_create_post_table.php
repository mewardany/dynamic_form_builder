<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180119_124427_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('post', [
            'id' => $this->primaryKey(),
        ], $tableOptions);
        
        $this->createTable('post_details', [
            'id' => $this->primaryKey(),
            'post_id'=> $this->integer(),
            'input_id'=> $this->integer(),
            'value'=> $this->text()
        ], $tableOptions);
        
        $this->addForeignKey('post_details_post_fk', 'post_details', 'post_id', 'post', 'id');
        $this->addForeignKey('post_details_input_fk', 'post_details', 'input_id', 'dynamic_form_inputs', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('post_details_post_fk', 'post_details');
        $this->dropForeignKey('post_details_input_fk', 'post_details');
        $this->dropTable('post_details');
        $this->dropTable('post');
    }
}

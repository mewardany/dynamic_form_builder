<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model wardany\dform\models\DynamicFormInput */

$this->title = 'Create Dynamic Form Input';
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Form Inputs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-form-input-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

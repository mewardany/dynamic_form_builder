<?php

namespace wardany\dform;
use yii\base\InvalidConfigException;

/**
 * dynamic_form module definition class
 */
class DynamicFormViewer extends \yii\base\Module
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'wardany\dform\controllers';

    public $upload_url;

    /**
     * @inheritdoc
     */
    public function init(){
        parent::init();
        if (empty($this->upload_url)) {
            throw new InvalidConfigException('"upload_url" property must be set.');
        }
    }
}

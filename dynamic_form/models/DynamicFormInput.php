<?php

namespace wardany\dform\models;

use wardany\dform\helpers\FieldHelper;

/**
 * This is the model class for table "dynamic_form_inputs".
 *
 * @property int $id
 * @property int $form_id
 * @property int $type
 * @property string $name
 * @property string $label
 * @property string $input_data
 * @property string $html_attributes_options
 * @property string $validation_rules
 *
 * @property DynamicForm $form
 */
class DynamicFormInput extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dynamic_form_inputs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id', 'type'], 'required'],
            [['name', 'label'], 'required'],
            [['form_id', 'type'], 'integer'],
            [['input_data', 'html_attributes_options', 'validation_rules'], 'string'],
            [['name', 'label'], 'string', 'max' => 255],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => DynamicForm::className(), 'targetAttribute' => ['form_id' => 'id']],
            ['type', 'in', 'range'=> array_keys(FieldHelper::fields())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_id' => 'Form ID',
            'type' => 'Type',
            'name' => 'Name',
            'label' => 'Label',
            'input_data' => 'Input Data',
            'html_attributes_options' => 'Html Attributes Options',
            'validation_rules' => 'Validation Rules',
        ];
    }

    public function behaviors() {
        return[
            ValidationRule::className()
        ];
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(DynamicForm::className(), ['id' => 'form_id']);
    }
    
    public function getInputType() {
        return FieldHelper::field($this->type)['title'];
    }
    
    public function getInputIcon() {
        return \yii\helpers\Html::tag('span', null, ['class'=> FieldHelper::field($this->type)['icon']]);
    }
}

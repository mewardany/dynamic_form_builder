<?php

namespace wardany\dform\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "dynamic_form".
 *
 * @property int $id
 * @property string $title
 * @property string $note
 * @property string $extra_validation_rules
 * @property string $custom_form_file
 * @property string $custom_search_file
 * @property string $custom_view_file
 *
 * @property DynamicFormInputs[] $dynamicFormInputs
 */
class DynamicForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dynamic_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['note', 'extra_validation_rules'], 'string'],
            [['title', 'custom_form_file', 'custom_search_file', 'custom_view_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'note' => 'Note',
            'extra_validation_rules' => 'Extra Validation Rules',
            'custom_form_file' => 'Custom Form File',
            'custom_search_file' => 'Custom Search File',
            'custom_view_file' => 'Custom View File',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDynamicFormInputs()
    {
        return $this->hasMany(DynamicFormInputs::className(), ['form_id' => 'id']);
    }
    
    public static function getCustomFormFiles() {
        $module = Yii::$app->getModule('dynamic_form_builder');
        $path = Yii::getAlias($module->custom_form_files_path);
        if(is_dir($path)){
            $files = FileHelper::findFiles($path,['only'=>['*.php']]);
            $files_array =[];
            foreach ($files as $file) {
                $files_array[ pathinfo ($file,PATHINFO_BASENAME)]= pathinfo ($file, PATHINFO_FILENAME);
            }
            return $files_array;
        }
        return [];
    }
    
    public static function getCustomViewFiles() {
        $module = Yii::$app->getModule('dynamic_form_builder');
        $path = Yii::getAlias($module->custom_view_files_path);
        if(is_dir($path)){
            $files = FileHelper::findFiles($path,['only'=>['*.php']]);
            $files_array =[];
            foreach ($files as $file) {
                $files_array[ pathinfo ($file,PATHINFO_BASENAME)]= pathinfo ($file, PATHINFO_FILENAME);
            }
            return $files_array;
        }
        return [];
    }
    
    public static function getCustomSearchFiles() {
        $module = Yii::$app->getModule('dynamic_form_builder');
        $path = Yii::getAlias($module->custom_search_files_path);
        if(is_dir($path)){
            $files = FileHelper::findFiles($path,['only'=>['*.php']]);
            $files_array =[];
            foreach ($files as $file) {
                $files_array[ pathinfo ($file,PATHINFO_BASENAME)]= pathinfo ($file, PATHINFO_FILENAME);
            }
            return $files_array;
        }
        return [];
    }
}

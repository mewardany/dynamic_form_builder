<?php

namespace wardany\dform\models;

use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\validators\Validator;

/**
 * Description of ValidationRule
 *
 * @author Muhammad Wardany <muhammad.wardany@gmail.com>
 */
class ValidationRule extends \yii\base\Behavior{
    private $_validators = [
        'required'=> false,
        'string'=> false,
        'max_string_length'=> 255,
        'min_string_length'=> null,
        'string_length'=> null,
    ];
    
    public function events() {
        return [
            ActiveRecord::EVENT_INIT=> 'start',
            ActiveRecord::EVENT_AFTER_FIND=> 'afterFind',
            ActiveRecord::EVENT_BEFORE_INSERT=> 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE=> 'beforeInsert',
        ];
    }
    
    public function start() {
        $this->attachRules($this->owner);
    }
    
    public function afterFind() {
        $validators = Json::decode($this->owner->validation_rules);
        $this->_validators = array_merge($this->_validators, $validators);
    }
    
    public function beforeInsert() {
        $this->owner->validation_rules = Json::encode(array_filter($this->_validators));
        return true;
    }
        
    /**
     * 
     * @param ActiveRecord $owner
     */
    public function attachRules($owner) {
        $owner->validators[] = Validator::createValidator('boolean', $owner, ['required', 'string']);
        $owner->validators[] = Validator::createValidator('integer', $owner, ['max_string_length', 'min_string_length', 'string_length']);
    }
    
    public function canGetProperty($name, $checkVars = true) {
        return parent::canGetProperty($name, $checkVars) || array_key_exists($name, $this->_validators);
    }
    
    public function canSetProperty($name, $checkVars = true) {
        return parent::canSetProperty($name, $checkVars) || array_key_exists($name, $this->_validators);
    }
    
    public function __get($name) {
        try {
            parent::__get($name);
        } catch (UnknownPropertyException $exc) {
            if(array_key_exists($name, $this->_validators))
                return $this->_validators[$name];
            throw $exc;
        }
    }
    
    public function __set($name, $value) {
        try {
            parent::__set($name, $value);
        } catch (UnknownPropertyException $exc) {
            if(array_key_exists($name, $this->_validators))
                $this->_validators[$name] = $value;
            else
                throw $exc;
        }
    }
    
    public function loadRules($attribute_name) {
        $rules=[];
        $this->addRequiredRules($rules, $attribute_name);
        $this->addStringRules($rules, $attribute_name);
        return $rules;
    }
    
    public function addRequiredRules(&$rules, $attribute_name) {
        if($this->required)
            $rules[]= [$attribute_name, 'required'];
    }
    
    public function addStringRules(&$rules, $attribute_name) {
        if($this->string){
            $rule= [$attribute_name, 'string'];
            if(is_numeric($this->max_string_length))
                $rule['max']= $this->max_string_length;
            if(is_numeric($this->min_string_length))
                $rule['min']= $this->min_string_length;
            if(is_numeric($this->string_length))
                $rule['length']= $this->string_length;
            
            $rules[]= $rule;
        }            
    }
}

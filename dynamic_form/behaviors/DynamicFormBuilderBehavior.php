<?php
namespace wardany\dform\behaviors;

use yii\db\BaseActiveRecord;

/**
 * Description of DynamicFormBuilderBehavior
 *
 * @author Muhammad Wardany <muhammad.wardany@gmail.com>
 */
class DynamicFormBuilderBehavior extends \yii\base\Behavior{
    private $extra_attributes= [];
       
    public function events() {
        return[
            BaseActiveRecord::EVENT_INIT            => 'start',
//            BaseActiveRecord::EVENT_AFTER_FIND      => 'afterFind',
//            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
//            BaseActiveRecord::EVENT_AFTER_INSERT    => 'afterSave',
//            BaseActiveRecord::EVENT_AFTER_UPDATE    => 'afterSave',
//            BaseActiveRecord::EVENT_AFTER_DELETE    => 'afterDelete',
        ];
    }
    
    public function start() {
        $this->attachExtraAttributes();
    }
    
    public function attachExtraAttributes() {
        $extra_attributes = \wardany\dform\models\DynamicFormInput::find()
                ->alias('inputs')
                ->leftJoin(['values'=>'post_details'], 'inputs.id = values.input_id')
                ->where(['inputs.form_id'=> $this->owner->form_id, 'values.post_id'=> $this->owner->id])
                ->select(['values.value', 'inputs.name'])
                ->indexBy('name')
                ->column();
        \yii\helpers\VarDumper::dump($extra_attributes);
        die();
    }
    
//    public function canGetProperty($name, $checkVars = true) {
//        return  true;
//        parent::canGetProperty($name, $checkVars);
//    }
    
    public function __get($name) {
        die($name); 
       parent::__get($name);
    }
    
}

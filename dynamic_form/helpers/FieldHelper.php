<?php

namespace wardany\dform\helpers;

/**
 *
 * @author muhammad wardany <muhammad.wardany@gmail.com>
 */

use Yii;
use wardany\dform\fields_models;

class FieldHelper extends \yii\base\Component{

    const TEXT_INPUT = 1;
    const TEXT_AREA = 2;
    const EMAIL = 3;
    const URL = 4;
    const NUMBER = 5;
    const PHONE = 6;
    const CHECKBOX_LIST = 7;
    const DROPDOWN_LIST = 8;
    const RADIO_LIST = 9;
    const FILE = 10;
    const IMAGE = 11;
    const DATE_TIME = 12;

    public static function getInstance($type) {
        $input= self::field($type);
        $input_class = $input['class'];
        return new $input_class();
    }

    public static function fields() {
        return[
            self::TEXT_INPUT=>[
                'title'=> 'Text input',
                'class'=> fields_models\TextInput::className(),
                'icon'=>'glyphicon glyphicon-text-width',
                'form'=> '__textinput',
            ],
//            self::TEXT_AREA=>[
//                'title'=> 'Textarea',
//                'class'=> fields\FieldTextarea::className(),
//                'icon'=>'glyphicon glyphicon-edit',
//                'form'=> '__textarea',
//            ],
//            self::EMAIL=>[
//                'title'=> 'Url field',
//                'class'=> fields\FieldUrl::className(),
//                'icon'=>'glyphicon glyphicon-link',
//                'form'=> '__url',
//            ],
//            self::URL=>[
//                'title'=> 'Email field',
//                'class'=> fields\FieldEmail::className(),
//                'icon'=>'glyphicon glyphicon-envelope',
//                'form'=> '__email',
//            ],
//            self::NUMBER=>[
//                'title'=> 'Number field',
//                'class'=> fields\FieldNumber::className(),
//                'icon'=>'glyphicon glyphicon-shopping-cart',
//                'form'=> '__number',
//            ],
//            self::PHONE=>[
//                'title'=> 'Phone field',
//                'class'=> fields\FieldPhone::className(),
//                'icon'=>'glyphicon glyphicon-earphone',
//                'form'=> '__phone',
//            ],
//            self::CHECKBOX_LIST=>[
//                'title'=> 'Checkbox List',
//                'class'=> fields\FieldCheckboxList::className(),
//                'icon'=> 'glyphicon glyphicon-text-width',
//                'form'=> '__checkboxlist',
//            ],
//            self::DROPDOWN_LIST=>[
//                'title'=> 'Radio list',
//                'class'=> fields\FieldRadioList::className(),
//                'icon'=>'glyphicon glyphicon-record',
//                'form'=> '__radiolist',
//            ],
//            self::RADIO_LIST=>[
//                'title'=> 'Dropdown list',
//                'class'=> fields\FieldDropdownList::className(),
//                'icon'=>'glyphicon glyphicon-collapse-down',
//                'form'=> '__dropdownlist',
//            ],
//            
//            'file'=>[
//                'title'=> 'File field'),
//                'class'=> fields\FieldFile::className(),
//                'icon'=>'glyphicon glyphicon-folder-open',
//                'form'=> '__file',
//            ],
//            'image'=>[
//                'title'=> 'Image field'),
//                'class'=> fields\FieldImage::className(),
//                'icon'=>'glyphicon glyphicon-picture',
//                'form'=> '__image',
//            ],
        ];
    }
    
    public static function field($id) {
        $fields = self::fields();
        if(array_key_exists($id, $fields))
            return $fields[$id];
    }
}

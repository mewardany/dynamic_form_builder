<?php

namespace wardany\dform\fields_models;

/**
 * Description of newPHPClass
 *
 * @author muhammad wardany {muhammad.wardany@gmail.com}
 */
class TextInput extends \wardany\dform\models\DynamicFormInput {
    public function __construct($config = array()) {
        $this->type = \wardany\dform\helpers\FieldHelper::TEXT_INPUT;
        parent::__construct($config);
    }
}
